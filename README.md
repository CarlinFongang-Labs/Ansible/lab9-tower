# Ansible | Deploiement d'application avec Tower

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Dans ce lab, nous allons déployer une application à l'aide de Tower

## Objectifs

- Installation de Ansible Tower (AWX)
- Configuration d'un job (Project - Inventory - Credential - Template) 
- Confiugration d'un webhook sous github

## Prérequis
Disposer de deux machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

**Documentation complémentaire :**

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

[Galaxy ansible](https://galaxy.ansible.com/ui/standalone/roles/?page=1&page_size=100&sort=-created&keywords=wordpress)


## 1. Création des instances EC2

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

>![alt text](img/image.png)
*Provisionnement d'un instance sous CentOS 7*

## 2. Installation de tower

1. Prérequis

````bash
sudo apt update -y
````

2. Installation de docker

````bash
#!/bin/bash

## 1. Package update
sudo apt list --upgradable
sudo apt update -y


## 2. Installation of dependencies for Docker
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

## 3. Adding the official Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

## 4. Adding the Docker repository to APT sources
echo "" | sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

## 5. Updating the package list
sudo apt update -y

## 6. Docker installation
sudo apt install -y docker-ce

## 7. Checking the status of Docker
#sudo systemctl status docker

## 8. Adding the user to the Docker group
sudo usermod -aG docker $USER
sudo systemctl start docker
sudo systemctl enable docker

## 9. Git installation
sudo apt install -y git

echo "Docker et Git ont été installés avec succès."
````

3. Installation de docker compose

````bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
````

5. Téléchargement de AWX

````bash
git clone https://gitlab.com/CarlinFongang-Labs/Ansible/tower.tar.gz.git
cd tower.tar.gz/ 
tar xvf awx.tar.gz -C ~/
````

6. Installation d'AWX

````bash
cd ~/.awx/awxcompose/
````


une fois le fichier modifié, entrer la commande suivante pour lancer l'installation

````bash
docker-compose up -d
````

L'installation peut prendre un certain temps à se faire.

````bash
docker ps -a
````

>![alt text](img/image-12.png)
*Ensemble de conteneur en cours pour **awx***


## 3. Connexion à tower

depuis le navigateur on va entrer l'ip du serveur tower
````bash
http://44.209.106.60/
```` 

>![alt text](img/image-1.png)
*interface de connexion à tower*

les information de login sont dans notre cas : 
user : admin
password : password


>![alt text](img/image-2.png)
*Interface utilisateur tower*

## 4. Project, inventory & credentials

### 4.1. Création d'un projet

dépuis l'interface d'administration de tower : 

**Project > +**

1. Renseigner les informations et enregistrer

>![alt text](img/image-3.png)
*Le projet passe au vert*

2. Consultation des détails de création du projet

En cliquant sur la "puce verte" sur le projet, j'ai accès aux informations de celui-ci

>![alt text](img/image-4.png)
*Détails du projet*

### 4.2. Gestion de l'inventaire 

**Inventaires > + > Inventaire**

1. Nom du projet

>![alt text](img/image-5.png)
*Nom de l'inventaire*
 
Une fois le nom renseigné, on va enreistré

2. Sources

L'on va crée une nouvelle source en cliquant le bouton  **source > +**

>![alt text](img/image-13.png)

On renseigne les informations suivantes : 

````bash
NOM: client
SOURCE: Provenance d'un projet
FICHIER D'INVENTAIRE: hosts.yml
````

>![alt text](img/image-6.png)
*Mise à jour de la source d'inventaire*

>![alt text](img/image-14.png)
*La source d'inventaire est mis à jour*

L'on va ensuite cliqué sur le bouton **HÔTES** prèt de **SOURCES**

>![alt text](img/image-7.png)
*Dans la section Hôtes, on peut voir les details de hôtes tels que le groupe ou le nom d'hote défini dans ansible*

### 4.3. Définition des credentials

Information d'identification > +

1. Credential : key Id & secret key
Dans les chaps qui s'affiche on choisie le provider et l'on renseigne les informations

>![alt text](img/image-8.png)


2. Configuration de la **ssh key**

>![alt text](img/image-9.png)

compléter le champs **CLÉ PRIVÉE SSH** par le contenu de la clé privé ssh du client ec2


### 4.4. Création d'un job

**Modèles > + > Modèle de Job**

1. Informations pour la création du job

>![alt text](img/image-10.png)

2. Lancement du job

>![alt text](img/image-11.png)

>![alt text](img/image-15.png)
*Exécution du job*

>![alt text](img/image-16.png)
*Vue de l'ensemble des jobs*

### 4. Test de l'application

En entrant l'adresse ip de l'instance cible dans le navigateur on à accès à l'application

>![alt text](img/image-17.png)


## Configuration du webhook Github + Tower

1. Générer un token d'authentification Github

Dépuis le repository github, se rendre dans :

Settings > Developer settings

>![alt text](img/image-18.png)
*Settings*

>![alt text](img/image-19.png)
*Developer settings*

>![alt text](img/image-20.png)
*Personal access tokens > Generate new token*

delà on va crée un token classic 

>![alt text](img/image-21.png)

Une fois les informations renseignée, générer le tocken : 

````bash
TOKEN : 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
````

2. Configuration du token depuis tower
Credential > + > 

renseigner les informations : 

````bash
NOM : awx-access-token
TYPE : D'INFORMATION D'IDENTIFICATION: Github Personnal Token
JETON : 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
````

>![alt text](img/image-22.png)

3. Configuration du Webhook depuis tower

Modèles > DeployApp > cocher la case **webhook**

````bash
WEBHOOK SERVICE: GitHub
WEBHOOK CREDENTIAL: awx-access-token
````

>![alt text](img/image-23.png)

4. Configuration du webhook GitHub

l'on va récupérer l'URL webhook présent dans tower 

>![alt text](img/image-24.png)
*url webhook*

L'on reviens ensuite sur github.com > tower-ansible-demo > Settings > Webhooks > Add webhook

````bash
Payload URL: http://44.221.157.195:80/api/v2/job_templates/10/github/
Secret: JFjRHqlyUM3mbs8kMs4U1xbvOXVLA4mnakCsHgmhZTeZNbVFLN
````
le secret se trouve pret de l'URL du playbook dans tower

>![alt text](img/image-25.png)

une fois configuré, on enregistre le tout et on test le webhook configuré en envoyant un evènement à tower

>![alt text](img/image-26.png)
*test webhook validé*

Une fois le webhook github configuré, toute modification apportée sur le repo concerné entraine automatiquement une exécution du job au niveau de tower et un redeploiement de l'application.

